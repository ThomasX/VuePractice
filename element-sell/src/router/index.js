import Vue from 'vue';
import Router from 'vue-router';
import Goods from '../components/goods/goods';
import Seller from '../components/seller/seller';
import Ratings from '../components/ratings/ratings';

Vue.use(Router);

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: '/goods'
    },
    {
      path: '/goods',
      name: '商品',
      component: Goods
    },
    {
      path: '/ratings',
      name: '评价',
      component: Ratings
    },
    {
      path: '/seller',
      name: '商家',
      component: Seller
    }
  ]
})
