import Vue from 'vue';
import App from './App';
import router from './router';
import axios from 'axios';
// import './common/styles/index.less';
require('./common/styles/index.less');


Vue.config.productionTip = false;
Vue.prototype.$http = axios;

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
